local util = require "lspconfig/util"
local opts = { noremap=true, silent=true }
---vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
---vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
---vim.keymap.set('n', 'jd', vim.diagnostic.goto_next, opts)
---vim.keymap.set('n', '<space>q', vim.diagnostics.setloclist, opts)


local on_attach = function(client, bufnr)
	-- Enable completion triggered by <c-x><c-o>
  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap=true, silent=true, buffer=bufnr }
  vim.keymap.set('n', 'gD', vim.lsp.buf.declaration, bufopts)
  vim.keymap.set('n', 'gd', vim.lsp.buf.definition, bufopts)
  vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
  vim.keymap.set('n', 'gi', vim.lsp.buf.implementation, bufopts)
  vim.keymap.set('n', '<C-k>', vim.lsp.buf.signature_help, bufopts)
  vim.keymap.set('n', '<space>wa', vim.lsp.buf.add_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wr', vim.lsp.buf.remove_workspace_folder, bufopts)
  vim.keymap.set('n', '<space>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, bufopts)
  vim.keymap.set('n', '<space>D', vim.lsp.buf.type_definition, bufopts)
  vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<space>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<space>f', function() vim.lsp.buf.format { async = true } end, bufopts)
end


require'lspconfig'.pyright.setup{
	on_attach = on_attach
}
require'lspconfig'.gopls.setup {
    	on_attach = on_attach,
    	cmd = { "/home/alek/go/bin/gopls" }
}
require'lspconfig'.ccls.setup {
    on_attach = on_attach,
    init_options = {
        cache = {
            directory = "/tmp/ccls-cache";
        };
    }
}


local runtime_path = vim.split(package.path, ":")
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?init.lua")

require'lspconfig'.lua_ls.setup {
    on_attach = on_attach,
    settings = {
        Lua = {
            runtime = {
                version = "LuaJIT",
                path = runtime_path,
            },
            diagnostics = {
                globals = { "vim" },
            },
            workspace = {
                 library = vim.api.nvim_get_runtime_file("", true),
		 checkThirdParty = false,

            },
            telemetry = {
                enable =  false,
            },
        },
    },
}

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

require'lspconfig'.html.setup {
	capabilities = capabilities,
	on_attach = on_attach,
	embeddedLanguages = {
		css = true,
		javascript = true,
	}
}

 require'lspconfig'.cssls.setup {
}

require'lspconfig'.tsserver.setup{}
